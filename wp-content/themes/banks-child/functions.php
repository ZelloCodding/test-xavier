<?php

/**
 * Recommended way to include parent theme styles.
**/

add_action( 'wp_enqueue_scripts', 'banks_child_assets' );
    function banks_child_assets() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('parent-style') );
        wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
        wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri() . '/css/slick.css');
        wp_enqueue_style( 'slick-theme-css', get_stylesheet_directory_uri() . '/css/slick-theme.css');
        wp_enqueue_style( 'bank-css', get_stylesheet_directory_uri() . '/css/bank.css');
        wp_enqueue_style( 'currency-widget-css', 'https://www.ifcmarkets.com/css/widget/live_quotes_ifc_widget.css');
        wp_deregister_script('jquery');
        wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true );
        wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), null, true );
        wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/js/slick.min.js', array('jquery'), null, true );
        wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), null, true );
}

/**
 * Custom post type install for this theme.
 **/

require get_theme_file_path('inc/cpt-install.php');

function archive_meta_query( $query ) {

    if( $query->is_post_type_archive('bank') ){
        if(isset($_GET['submit_filter'])) {
            $meta_query = array('relation' => 'AND');

            if($bank_name = $_GET['bank_name']) {
                $query->query_vars["s"] = $bank_name;
            }

            if($bank_descr = $_GET['bank_descr']) {
                $meta_query[] = array(
                    'key' => 'description',
                    'value' => $bank_descr,
                    'compare' => 'LIKE',
                );
            }

            $rating_start = intval($_GET['bank_rating_start']);
            $rating_end = intval($_GET['bank_rating_end']);

            if(!empty($rating_start) && !empty($rating_end)) {
                $meta_query[] = array(
                    'key' => 'rating',
                    'value' => array($rating_start, $rating_end),
                    'compare' => 'BETWEEN',
                    'type'    => 'numeric'
                );
            }elseif(empty($rating_start) && !empty($rating_end)) {
                $meta_query[] = array(
                    'key' => 'rating',
                    'value' => $rating_end,
                    'compare' => '<=',
                    'type'    => 'numeric'
                );
            }elseif(!empty($rating_start) && empty($rating_end)) {
                $meta_query[] = array(
                    'key'     => 'rating',
                    'value'   => $rating_start,
                    'compare' => '>=',
                    'type'    => 'numeric'
                );
            }

            $capital_start = intval($_GET['bank_capital_start']);
            $capital_end = intval($_GET['bank_capital_end']);

            if(!empty($capital_start) && !empty($capital_end)) {
                $meta_query[] = array(
                    'key' => 'capitalization',
                    'value' => array($capital_start, $capital_end),
                    'compare' => 'BETWEEN',
                    'type'    => 'numeric'
                );
            }elseif(empty($capital_start) && !empty($capital_end)) {
                $meta_query[] = array(
                    'key' => 'capitalization',
                    'value' => $capital_end,
                    'compare' => '<=',
                    'type'    => 'numeric'
                );
            }elseif(!empty($capital_start) && empty($capital_end)) {
                $meta_query[] = array(
                    'key' => 'capitalization',
                    'value' => $capital_start,
                    'compare' => '>=',
                    'type'    => 'numeric'
                );
            }

            $query->set('meta_query',$meta_query);

        }
    }
}
add_action( 'pre_get_posts', 'archive_meta_query', 1 );
