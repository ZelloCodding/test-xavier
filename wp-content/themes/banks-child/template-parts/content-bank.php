<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Banks
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="article__image">
        <?php banks_post_thumbnail(); ?>
    </div>

    <?php the_title( '<h2 class="article__title">', '</h2>' ); ?>

    <?php if($descr = get_post_meta(get_the_ID(), 'description', false)) : ?>
        <div class="article__content">
            <?php echo substr(strip_tags($descr[0]), 0, 150) . '...'; ?>
        </div><!-- .entry-content -->
    <?php endif; ?>

    <a class="btn btn-primary article__more" href="<?php the_permalink(); ?>">View more</a>

</article><!-- #post-<?php the_ID(); ?> -->
