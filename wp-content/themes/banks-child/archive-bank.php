<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Banks
 */

get_header();

$bank_name = '';
$bank_descr = '';
$bank_rating_start = '';
$bank_rating_end = '';
$bank_capital_start = '';
$bank_capital_end = '';

if(isset($_GET['submit_filter']) && !empty($_GET['submit_filter'])) {
    $bank_name = $_GET['bank_name'] ? $_GET['bank_name'] : '';
    $bank_rating_start = $_GET['bank_rating_start'] ? $_GET['bank_rating_start'] : '';
    $bank_capital_start = $_GET['bank_capital_start'] ? $_GET['bank_capital_start'] : '';
    $bank_descr = $_GET['bank_descr'] ? $_GET['bank_descr'] : '';
    $bank_rating_end = $_GET['bank_rating_end'] ? $_GET['bank_rating_end'] : '';
    $bank_capital_end = $_GET['bank_capital_end'] ? $_GET['bank_capital_end'] : '';
}

?>

	<div id="primary" class="content-area news-archive">

        <form id="banks-filter" class="banks-filter">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="bank_name">Bank name</label>
                            <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Enter bank name" value="<?php echo strip_tags($bank_name); ?>">
                            <small id="emailHelp" class="form-text text-muted">You may enter any part of name.</small>
                        </div>
                        <div class="form-group">
                            <label for="bank_rating_start">Bank rating from</label>
                            <input type="number" class="form-control" id="bank_rating_start" name="bank_rating_start" placeholder="123456" value="<?php echo intval($bank_rating_start); ?>">
                        </div>
                        <div class="form-group">
                            <label for="bank_capital_start">Bank capitalization from</label>
                            <input type="number" class="form-control" id="bank_capital_start" name="bank_capital_start" placeholder="Enter capitalization" value="<?php echo intval($bank_capital_start); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="bank_descr">Bank description</label>
                            <input type="text" class="form-control" id="bank_descr" name="bank_descr" placeholder="Enter description" value="<?php echo strip_tags($bank_descr); ?>">
                            <small id="emailHelp" class="form-text text-muted">You may enter any part of description.</small>
                        </div>
                        <div class="form-group">
                            <label for="bank_rating_end">Bank rating to</label>
                            <input type="number" class="form-control" id="bank_rating_end" name="bank_rating_end" placeholder="123456" value="<?php echo intval($bank_rating_end); ?>">
                        </div>
                        <div class="form-group">
                            <label for="bank_capital_end">Bank capitalization to</label>
                            <input type="number" class="form-control" id="bank_capital_end" name="bank_capital_end" placeholder="Enter capitalization" value="<?php echo intval($bank_capital_end); ?>">
                        </div>
                        <div class="form-group">
                            <input type="submit" id="submit_filter" name="submit_filter" class="btn btn-primary" value="Filter">
                            <a href="<?php echo site_url() . '/bank'; ?>" id="reset_filter" class="btn btn-error">Reset</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

            <div class="container">

                <header class="page-header">
                    <?php
                    the_archive_title( '<h1 class="page-title">', '</h1>' );
                    the_archive_description( '<div class="archive-description">', '</div>' );
                    ?>
                </header><!-- .page-header -->

                <div class="row">

                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) :
                        the_post();

                        /*
                         * Include the Post-Type-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                         */
                        echo '<div class="col-md-4">';
                        get_template_part( 'template-parts/content', get_post_type() );
                        echo '</div>';

                    endwhile; ?>

                    </div>
                </div>

                 <?php
                    the_posts_pagination();

                else :

                    get_template_part( 'template-parts/content', 'none' );

                endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
