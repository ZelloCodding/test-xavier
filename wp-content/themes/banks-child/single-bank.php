<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Banks
 */

get_header();

$post_id = get_the_ID();
?>

	<div id="primary" class="content-area banks-list">

		<main id="main" class="site-main">
            <div class="container">
                <div class="row">
                    <div class="article-single col-md-12">
                        <div class="article-single__img">
                            <?php the_post_thumbnail( 'full'); ?>
                        </div>
                        <?php the_title( '<h1 class="article-single__title">', '</h1>' ); ?>
                        <div class="article-single__meta">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php if($bank_rating = get_post_meta($post_id, 'rating', true)) : ?>
                                        <div>
                                            <span class="label">Rating: </span>
                                            <span class="value"><?php echo $bank_rating; ?></span>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($bank_capitalization = get_post_meta($post_id, 'capitalization', true)) : ?>
                                        <div>
                                            <span class="label">Capitalization: </span>
                                            <span class="value">$<?php echo $bank_capitalization; ?></span>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($bank_link = get_post_meta($post_id, 'link_for_bank', true)) : ?>
                                        <div>
                                            <span class="label">Link: </span>
                                            <span class="value"><a href="<?php echo $bank_link; ?>"><?php echo $bank_link; ?></a></span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-6">
                                    <script type="text/javascript">var lang='en';var hname="www.ifcmarkets.com";var bid = 'Bid';var ask = 'Ask';var vi=document.createElement('script');vi.type='text/javascript';vi.async = true;vi.src = 'https://www.ifcmarkets.com/js/live_quotes_ifc_widget.js';var instrument_list="EURUSD,GBPUSD,USDPLN,USDRUB,XAGUSD,XAUUSD";var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(vi, s);</script>
                                    <div id="ifc_widgetlivequotes" class="ifc_widgetlivequotes"></div><div id="ifc_nedlivequotes" class="ifc_nedlivequotes"><a href="https://www.ifcmarkets.com/en/informers" target="blank" rel="nofollow">Provided</a> by IFC Markets</div>
                                </div>
                            </div>
                        </div>
                        <?php if($bank_descr = get_post_meta($post_id, 'description', true)) : ?>
                            <div class="article-single__descr"><?php echo $bank_descr; ?></div>
                        <?php endif; ?>

                        <?php if($bank_gallery = get_post_meta($post_id, 'gallery_data', true)) : ?>
                            <div class="article-single__gallery">
                                <?php foreach($bank_gallery['image_url'] as $image) : ?>
                                    <div class="article-single__gallery-item">
                                        <img src="<?php echo $image; ?>" alt="gallery">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <?php the_post_navigation(); ?>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
